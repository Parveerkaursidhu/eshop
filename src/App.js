import React, { Component } from 'react';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import Layout from './components/Layout'
import Carousel from './components/Carousel';
import Cards from './components/Cards';
import Shop from '../src/components/Shop'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Footer from './components/Footer';

function App() {
  return (
    <div>
    <Router>
      <Switch>
      <Route exact path="/shop" component={Shop} />
      {/* <Route exact path="/" component={Home} />
      <Route exact path="/" component={Home} />
      <Route exact path="/" component={Home} />
      <Route exact path="/" component={Home} /> */}
      </Switch>
    </Router>

  
      <Layout/>
      <Carousel/>
      <Cards/>
      <Footer/>

    </div>
  );
    
  
      


     
  
  
}

export default App;
