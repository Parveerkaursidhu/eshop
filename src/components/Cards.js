import React from 'react';
import '../css/Cards.css';

export default function Cards() {
    return (
        <div className="container-fluid">
        <div className="container">
            <div class="card card-main">
                <img class="card-img-top" src="https://www.google.com/search?q=images&safe=active&rlz=1C1EJFA_enCA778CA780&sxsrf=ALeKk00h6vYW997NExm2Iqqtflyb5G_EnQ:1604939105642&tbm=isch&source=iu&ictx=1&fir=iNqV2FgquCb8DM%252ClbUiHZWH7L7PkM%252C_&vet=1&usg=AI4_-kRAjGAIv5sd6p8_9ogVgxmsLKOqEQ&sa=X&ved=2ahUKEwjZi5yK8PXsAhUbV80KHT9LCa8Q9QF6BAgPEFo#imgrc=iNqV2FgquCb8DM" alt="Card image cap"></img>
                        <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
            </div>  
            <div class="card card-main">
                <img class="card-img-top" src="..." alt="Card image cap"></img>
                        <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
            </div>  
            <div class="card card-main">
                <img class="card-img-top" src="..." alt="Card image cap"></img>
                        <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
            </div>  
            <div class="card card-main">
                <img class="card-img-top" src="..." alt="Card image cap"></img>
                        <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
            </div>  
            <div class="card card-main">
                <img class="card-img-top" src="..." alt="Card image cap"></img>
                        <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
            </div>  
            <div class="card card-main">
                <img class="card-img-top" src="..." alt="Card image cap"></img>
                        <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
            </div>  
           
        </div>
        </div>
    )
}