import React from 'react';
import '../css/Layout.css'
export default function Layout(){
    return(
       
<div className="container-fluid">

  <div class="row">
    <div class="col">
    <div class="col-xl-3 col-lg-2">
                    <div class="header__logo">
                        <a href="./index.html"><img src={require('../images/logo.png')} /></a>
                    </div>
                </div>
    </div>
    <div class="col-6" >
    <nav class="navbar navbar-expand-lg navbar-light ">
 
   <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
   <div class="navbar-nav">
     <a class="nav-item nav-link active" href="#">HOME <span class="sr-only">(current)</span></a>
     <a class="nav-item nav-link" href="#">WOMEN'S</a>
     <a class="nav-item nav-link" href="#">MEN'S</a>
     <a class="nav-item nav-link" href="#">SHOP</a>
     <a class="nav-item nav-link" href="#">PAGES</a>
     <a class="nav-item nav-link" href="#">BLOG</a>
     <a class="nav-item nav-link" href="#">CONTACT</a>
   </div>
 </div>
</nav>
    </div>
    <div class="col login">
     <a class="nav-item " href="#">Login</a>
     <span>|</span>
     <a class="nav-item " href="#">Register</a>
    </div>
  </div>

  
</div>

    );
}