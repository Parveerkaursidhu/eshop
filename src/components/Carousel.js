import React from 'react';
import '../css/Carousel.css';

export default function Carousel (){
  
    return(
        <div className='carousel-main'>
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      {/* <img src="..." class="d-block w-100" alt="Image 1"/> */}
      <img  class="d-block w-100" alt="Image 1" src={'../images/sky.jpg'} />
    </div>
    <div class="carousel-item">
      <img src="..." class="d-block w-100" alt="Image 2"/>
    </div>
    <div class="carousel-item">
      <img src="..." class="d-block w-100" alt="Image 3"/>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
        </div>
    );
}